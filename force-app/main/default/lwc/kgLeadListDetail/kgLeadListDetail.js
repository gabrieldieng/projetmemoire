import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import convertLead from '@salesforce/apex/KG_PropertyCnt.convertLead';
import getLeadById from '@salesforce/apex/KG_PropertyCnt.getLeadById';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class KgLeadListDetail extends LightningElement {
    @track selectedStep = 'Select1';
    @track lead = {};
    @track status;
    @track canConvert = false;
    @api recordId; // = '00Q3X00001Cj1nLUAR';
    @wire(getLeadById, {
        leadId: '$recordId'
    })
    callBack(result) {
        if (result.data) {
            var response = result.data;
            if (!response.hasError) {
                this.lead = response.lead;
                console.log('lead', this.lead);
            } else {
                console.log('error', response.message);
            }
        } else {
            console.log('error', result.error);
        }
    }

    @track selectedStep = 'Select1';
 
    handleNext() {
        var getselectedStep = this.selectedStep;
        if(getselectedStep === 'Select1'){
            this.selectedStep = 'Select2';
        }
        else if(getselectedStep === 'Select2'){
            this.selectedStep = 'Selet3';
        }
        else if(getselectedStep === 'Select3'){
            this.selectedStep = 'Select4';
        }
    }
 
    handlePrev() {
        var getselectedStep = this.selectedStep;
        if(getselectedStep === 'Step2'){
            this.selectedStep = 'Step1';
        }
        else if(getselectedStep === 'Step3'){
            this.selectedStep = 'Step2';
        }
        else if(getselectedStep === 'Step4'){
            this.selectedStep = 'Step3';
        }
    }

    selectStep1() {
        if (!this.canConvert) {
            this.canConvert = true;
        }
        this.selectedStep = 'Select1';
    }

    selectStep2() {
        if (!this.canConvert) {
            this.canConvert = true;
        }
        this.selectedStep = 'Select2';
    }

    selectStep3() {
        if (!this.canConvert) {
            this.canConvert = true;
        }
        this.selectedStep = 'Select3';
    }

    selectStep4() {
        if (!this.canConvert) {
            this.canConvert = true;
        }
        this.selectedStep = 'Select4';
    }

    get isSelectStep4() {

        return this.selectedStep === "Select4";
    }


    convertLead() {
        convertLead({
                recordId: this.recordId
            })
            .then(result => {
                if (!result.hasError) {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Lead converti avec succès!!!',
                            variant: 'Success'
                        }));
                    console.log('Lead converted successfully');
                } else {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'error',
                            message: 'Lead non converti!!!',
                            variant: 'error'
                        }));
                    console.log('error', result.message);
                }
            })
            .catch(error => {
                console.log('error', error);
            })
    }
}