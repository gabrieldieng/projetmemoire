import { LightningElement,wire,track,api} from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import {getRecord} from 'lightning/uiRecordApi';
import sendMailMethod from '@salesforce/apex/KG_PropertyCnt.sendMailMethod';

const FIELDS = ['Lead.Email', 'Lead.Name'];

export default class KGleadActivity extends LightningElement {

@track showEmail=true;
@track showForm=false;
@track error ;
@track email; 
@track name;
lead;
@track subject='';
@track body='';
@api recordId;
@wire(getRecord, {
    recordId: '$recordId', fields: FIELDS
}) wiredRecord({ error, data }) {
    if (error) {
        let message = 'Unknown error';
        if (Array.isArray(error.body)) {
            message = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            message = error.body.message;
        }
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Error loading lead',
                message,
                variant: 'error',
            }),
        );
    } else if (data) {
        this.lead = data;
        this.name = this.lead.fields.Name.value;
        this.email = this.lead.fields.Email.value;
    }
}


    handleClick(){
        this.showEmail=false;
        this.showForm=true;
    }

    subjectChange(event){
        
        this.subject=event.target.value;
    }

    bodyChange(event){
        
        this.body=event.target.value;
    }

    envoyermail(){
        sendMailMethod({
            mMail:this.email,
            mSubject:this.subject,
            mbody:this.body
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Email envoyé avec succés',
                        variant: 'Success'
                    }));
                   
            })

            .catch(error => {
                // var errorSaveLead = error.body.pageErrors[0].statusCode+'   '+error.body.pageErrors[0].message;
                this.messageSuccess = '';
                this.messageError = 'Insertion Error: ' + error.body.message; //JSON.stringify(error);
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error',
                        message: 'Erreur d envoie: ' + error.body.message, //JSON.stringify(error),
                        variant: 'Error'
                    }));
                    
            });
            
    }
}