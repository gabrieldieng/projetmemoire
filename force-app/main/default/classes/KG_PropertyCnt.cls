/**
 * @File Name          : GK_PropertyCnt.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 23/03/2020 à 13:02:37
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    19/02/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class KG_PropertyCnt {
    @AuraEnabled(cacheable=true)
    public static Map<String, Object> getAllProperties(){
        /*if (!Product2.sObjectType.getDescribe().isAccessible()) {
            throw new AuraHandledException('not access');
        }*/
        List<Product2> allProperties = new List<Product2>();
        Map<String, Object> response = new Map<String, Object>{'hasError'=>false};
        try {
            for(List<Product2> properties : [SELECT Name, CreatedDate, Description, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_State__c, Billing_Street__c, product_price__c, product_type__c, prestation__c, Superficie__c, Nombre_Chambres__c, Nombre_Salles_De_Bain__c FROM Product2 WHERE RecordType.Name='Propriete']){
                allProperties.addAll(properties);
            }
            response.put('properties',allProperties);
            response.put('pictures',KG_PicturesCnt.getFilesByParents((new Map<Id, Product2>(allProperties)).keySet()));
        } catch (Exception ex) {
            response.put('hasError', true);
            response.put('errorMessage', ex.getMessage());
        }
        return response;
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, Object> getPropertyById(Id propertyId){
        if (!Product2.sObjectType.getDescribe().isAccessible()) {
            throw new AuraHandledException('not access');
        }
        Map<String, Object> response = new Map<String, Object>{'hasError'=>false};
        try {
            Product2 property = [SELECT Name, Description, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_State__c, Billing_Street__c, product_price__c, product_type__c, prestation__c, Superficie__c, Nombre_Chambres__c, Nombre_Salles_De_Bain__c,(SELECT Name, Superficie__c, product_type__c FROM Pieces__r) FROM Product2 WHERE Id =: propertyId AND RecordType.Name='Propriete'];
            GeoLocation geoInfo = new GeoLocation();
            Location loc = new Location();
            geoInfo.Street = property.Billing_Street__c;
            geoInfo.City = property.Billing_City__c;
            geoInfo.State =  property.Billing_State__c;
            geoInfo.PostalCode=property.Billing_Postal_Code__c;
            geoInfo.Country=property.Billing_Country__c;
            loc.icon ='action:map';
            loc.title = property.Name;
            loc.description = property.Description;
            loc.location = geoInfo; 
            response.put('location',new List<Location>{loc});
            response.put('property',property);
            response.put('pictures',KG_PicturesCnt.getFilesByParent(propertyId));
        } catch (Exception ex) {
            response.put('hasError', true);
            response.put('errorMessage', ex.getMessage());
        }
        return response;
    }

    public class Location{
        @AuraEnabled 
        public String icon{get;set;} 
        @AuraEnabled 
        public String title{get;set;} 
        @AuraEnabled
        public String description{get;set;} 
        @AuraEnabled 
        public GeoLocation location{get;set;} 
    }

    public class GeoLocation{
        @AuraEnabled 
        public String Street{get;set;}
        @AuraEnabled 
        public String City{get;set;}
        @AuraEnabled 
        public String State{get;set;}
        @AuraEnabled 
        public String PostalCode{get;set;}
        @AuraEnabled 
        public String Country{get;set;}
        
    }
    // @AuraEnabled
    // public static void saveLeadRecord(Account objAcc){
    //     try{
    //         insert objAcc;
    //     }
    //     catch(Exception ex) {
    //         throw new AuraHandledException(ex.getMessage());
    //     }
    // }

    @AuraEnabled
     public static void  saveLead(list<String> data)
     {
          String phone=data[2];
          String produit=data[9];
          list<Lead> test=new  list<Lead>();
          for(list<Lead>t:[select Id from Lead where Phone=:phone and Product__c=:produit])
          {
               test.addAll(t);
          }
if(test.size()==0)
          {
            Lead lead=new Lead();
            lead.firstName          = data[0];
            lead.lastName           = data[1];
            lead.Phone              = data[2];
            lead.Email              = data[3];
            lead.adress_perso__c    = data[4];
            lead.Company            = data[5];
            lead.addressCompany__c  = data[6];
            lead.Fax                = data[7];
            lead.Email              = data[8];
            lead.Product__c         = data[9];
            lead.Role__c            = data[10];
            lead.Status='Open - Not Contacted';
            insert lead;

            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
           
                  Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                  email.setToAddresses(new String[] {'marame.ndiaye@terangacloud.com','djibrildieng24@hotmail.fr'});
                  email.setSubject('Un nouveau client détecté');
                 email.setPlainTextBody('Je voudrais vous informé que un nouveau demandeur du prénom de ' +  lead.firstName +' et nom de  '+ lead.lastName + ' a fait une demande.');
                 emails.add(email);     
                  Messaging.sendEmail(emails);
            
                  List<Messaging.SingleEmailMessage> personnels = new List<Messaging.SingleEmailMessage>();
                        Messaging.SingleEmailMessage personnel = new Messaging.SingleEmailMessage();
                        personnel.setToAddresses(new String[] {lead.Email});
                        personnel.setSubject('Information Demande');
                       personnel.setPlainTextBody('Je voudrais vous informé que votre demande a été bien prise en charge, Vous receverez sous peu un appel d un de nos agents');
                       personnels.add(personnel);     
                        Messaging.sendEmail(personnels);
     
          }
          else {
               QueryException ex = new QueryException();
               ex.setMessage('Vous etes déja inscris sur ce numéro!');
               throw ex;
          }
     }


     








    
        @AuraEnabled(cacheable=true)
         public static List<Lead> getAllLeads() {
             return [SELECT Id, Name ,addressCompany__c, adress_perso__c ,Email ,EmailCompany__c,Fax,FaxCompany__c,Phone,phoneCompany__c,Product__c from Lead];
         }


         @AuraEnabled(cacheable=true)
         public static Map<String,Object> getLeadById(Id leadId) {
            Lead lead = null;
            Map<String,Object> response = new Map<String,Object>{'hasError'=>false};
            try {
                lead = [SELECT Id, Name, FirstName, LastName ,addressCompany__c, adress_perso__c ,Email ,EmailCompany__c,Fax,Company, FaxCompany__c,Phone,phoneCompany__c,Status, Product__c ,Product__r.Name, Product__r.product_price__c from Lead WHERE Id =: leadId];
                response.put('lead', lead);
            } catch (Exception ex) {
                response.put('hasError', true);
                
                response.put('message', ex.getMessage());
            }
            return response;
         }




    //convert lead to opportunity

    @AuraEnabled
    public static Map<String,Object> convertLead(Id recordId) {
        Lead lead = null;
        Map<String,Object> response = new Map<String,Object>{'hasError'=>false};
        try {
            lead = [Select Id,Name,Status,Company,product__c From lead WHERE Id=:recordId];
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(lead.id);
            lc.setopportunityname(lead.Company);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            //7. Lead conversion.
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            response.put('lead', lead);
        } catch (Exception ex) {
            response.put('hasError', true);
            response.put('message', ex.getMessage());
        }
        return response;
    }



   /*  @AuraEnabled
    public static Lead getStatus() {
        return [
            SELECT Id, Name, Company, Phone, Email, Status
            FROM Contact
            WHERE Picture__c != null
            WITH SECURITY_ENFORCED
            LIMIT 10
        ];
    } */



    @AuraEnabled
    public static void  saveLocation(list<String> data)
    {
         String phone=data[2];
         String produit=data[9];
         list<Lead> test=new  list<Lead>();
         for(list<Lead>t:[select Id from Lead where Phone=:phone and Product__c=:produit AND Product__r.Prestation__c='Location'])
         {
              test.addAll(t);
         }
if(test.size()==0)
         {
           Lead lead=new Lead();
           lead.firstName          = data[0];
           lead.lastName           = data[1];
           lead.Phone              = data[2];
           lead.Email              = data[3];
           lead.adress_perso__c    = data[4];
           lead.Company            = data[5];
           lead.addressCompany__c  = data[6];
           lead.Fax                = data[7];
           lead.Email              = data[8];
           lead.Product__c         = data[9];
           lead.Role__c            = data[10];
           lead.Status='Open - Not Contacted';
           insert lead;

           List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
          
                 Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                 email.setToAddresses(new String[] {'marame.ndiaye@terangacloud.com','djibrildieng24@hotmail.fr'});
                 email.setSubject('Un nouveau client détecté');
                email.setPlainTextBody('Je voudrais vous informé que un nouveau demandeur du prénom de ' +  lead.firstName +' et nom de  '+ lead.lastName + ' a fait une demande.');
                emails.add(email);     
                 Messaging.sendEmail(emails);
           
                 List<Messaging.SingleEmailMessage> personnels = new List<Messaging.SingleEmailMessage>();
                       Messaging.SingleEmailMessage personnel = new Messaging.SingleEmailMessage();
                       personnel.setToAddresses(new String[] {lead.Email});
                       personnel.setSubject('Information Demande');
                      personnel.setPlainTextBody('Je voudrais vous informé que votre demande a été bien prise en charge, Vous receverez sous peu un appel d un de nos agents');
                      personnels.add(personnel);     
                       Messaging.sendEmail(personnels);
    
         }
         else {
              QueryException ex = new QueryException();
              ex.setMessage('Vous etes déja inscris sur ce numéro!');
              throw ex;
         }
    }
     

}