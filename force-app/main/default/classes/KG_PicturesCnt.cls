/**
 * @File Name          : KG_PicturesCnt.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 20/02/2020 à 08:53:39
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/02/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class KG_PicturesCnt {
    public static List<Object> getFilesByParent(Id parentId) {
        return getFilesByParents(new Set<Id>{parentId});
    }

    public static List<sObject> getFilesByParents(Set<Id> parentsId){
        /*if (!ContentDocument.sObjectType.getDescribe().isAccessible() || !ContentVersion.sObjectType.getDescribe().isAccessible()) {
            throw new AuraHandledException('not access');
        }*/
        List<sObject> pictures = new List<sObject>();
        try {
            list<Id> relatedCnDocs = new list<Id>();
            for(ContentDocumentLink cntLink : [Select ContentDocumentId From ContentDocumentLink Where LinkedEntityId IN :parentsId]) {
                relatedCnDocs.add(cntLink.ContentDocumentId);
            }
            if(parentsId.size() == 1){
                if(!relatedCnDocs.isEmpty()) {
                    for(List<ContentVersion> listPictures: [SELECT Id, Title, ContentDocumentId, FileExtension FROM ContentVersion WHERE ContentDocumentId IN :relatedCnDocs]){
                        pictures.addAll(listPictures);
                    }
                }
            }
            else {
                for(List<sObject> listPictures: [SELECT Id, (SELECT Id FROM ContentVersions),(SELECT LinkedEntityId FROM ContentDocumentLinks) FROM ContentDocument WHERE Id IN :relatedCnDocs]){
                    pictures.addAll(listPictures);
                }
            }
        } catch(QueryException ex) {
            throw new QueryException(ex.getMessage());
        }
        return pictures;
    }
}